﻿using CCWin;
using OpenCvSharp;
using System;
using Point = OpenCvSharp.Point;

namespace WindowsFormsApp
{
    internal class FaceHandle
    {
        public FaceHandle()
        {
            
        }

        private CascadeClassifier faceCascade = new CascadeClassifier();
        private CascadeClassifier eyeCascade = new CascadeClassifier();

        public Mat findFace(Mat src)
        {
            try
            {
                initFaceXml();
                // 人脸检测
                Rect[] faces = faceCascade.DetectMultiScale(src, 1.3, 2, 0 | HaarDetectionType.ScaleImage, new OpenCvSharp.Size(30, 30));
                this.drawRect(faces, src);
            } 
            catch(Exception e)
            {
                return src;
            }

            return src;
        }

        private void initFaceXml()
        {

            try
            {
                if (!faceCascade.Load(@"./face/data/haarcascades/haarcascade_frontalface_alt.xml"))
                {
                    MessageBoxEx.Show("加载 haarcascade_frontalface_alt.xml 失败,请检查软件目录下是否存在/face/data/haarcascades/haarcascade_frontalface_alt.xml");
                    return;
                }
                if (!eyeCascade.Load(@"./face/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml"))
                {
                    MessageBoxEx.Show("加载 haarcascade_eye_tree_eyeglasses.xml 失败,请检查软件目录下是否存在/face/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml");
                    return;
                }
            } 
            catch(Exception ex)
            {
                MessageBoxEx.Show("加载 OpenCV模型失败,请检查软件目录下是否存在/face/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml与/face/data/haarcascades/haarcascade_frontalface_alt.xml");
                return;
            }
        }

        private Rect[] drawRect(Rect[] faces, Mat src)
        {
            for (int i = 0; i < faces.Length; i++)
            {
                // 绘制脸部区域
                Point center = new Point() { X = (faces[i].X + faces[i].Width / 2), Y = (faces[i].Y + faces[i].Width / 2) };
                Point center1 = new Point() { X = faces[i].X, Y = faces[i].Y };//矩形的点
                Point center2 = new Point() { X = (faces[i].X + faces[i].Width), Y = (faces[i].Y + faces[i].Height) };

                Cv2.Rectangle(src, center1, center2, Scalar.Green, 5, LineTypes.AntiAlias, 0);//绘制矩形
                                                                                               // Cv2.Rectangle(src, center1, center2, Scalar.Green, 5, LineTypes.AntiAlias, 0);//绘制矩形

                Rect[] eyes = eyeCascade.DetectMultiScale(src, 1.3, 2, 0 | HaarDetectionType.ScaleImage, new OpenCvSharp.Size(30, 30));
                for (int j = 0; j < eyes.Length; j++)
                {
                    Point eye_center = new Point() { X = (eyes[j].X + eyes[j].Width / 2), Y = (eyes[j].Y + eyes[j].Height / 2) };
                    int radius = (int)Math.Round((decimal)((eyes[j].Width + eyes[j].Height) * 0.25), 0, MidpointRounding.AwayFromZero);
                    Cv2.Circle(src , eye_center.X, eye_center.Y, radius, Scalar.Red, 3, LineTypes.Link8, 0);
                }
            }
            return faces;
        }
    }
}
