﻿using CCWin;
using System;
using System.Windows.Forms;

namespace WindowsFormsApp
{
    public partial class SaturationForm : CCSkinMain
    {
        public SaturationForm()
        {
            InitializeComponent();
        }

        public SaturationForm(MainForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            this.currentImage = mainForm.currrntImage;
            this.imageHandle = new ImageHandle();
            this.currentImage = mainForm.GetMainPictureBox().Image;
            this.staurationBar.ValueChanged += new EventHandler(handleBSaturation);
            this.hueBar.ValueChanged += new EventHandler(handleHue);
            this.okButton.Click += new EventHandler(handleOK);
            this.cancelButton.Click += new EventHandler(hndleCancel);
            this.FormClosed += new FormClosedEventHandler(this.FormClose);
        }

        /*
         * * 饱和度调整
         */
        public void handleBSaturation(object senser, EventArgs e)
        {
            this.mainForm.GetMainPictureBox().Image = this.imageHandle.saturation(this.currentImage, this.staurationBar.Value);
            this.tmpImage = this.mainForm.GetMainPictureBox().Image;
            this.satationLeb.Text = "饱和度" + this.staurationBar.Value.ToString() + "%";
        }

        /**
         * 色相调整
         */
        public void handleHue(object senser, EventArgs e)
        {
            this.mainForm.GetMainPictureBox().Image = this.imageHandle.hue(this.currentImage, this.hueBar.Value);
            this.tmpImage = this.mainForm.GetMainPictureBox().Image;
            this.hueLab.Text = "色相" + this.hueBar.Value.ToString();
        }

        /**
         * 监听确认
         */
        public void handleOK(object senser, EventArgs e)
        {
            this.mainForm.currrntImage = this.tmpImage;
            this.Close();
        }

        /**
         * 监听取消
         */
        public void hndleCancel(object senser, EventArgs e)
        {
            this.currentImage = this.mainForm.currrntImage;
            this.tmpImage = this.mainForm.currrntImage;
            this.mainForm.GetMainPictureBox().Image = this.currentImage;
            this.hueBar.Value = 0;
            this.staurationBar.Value = 0;
            this.Close();
        }

        private void FormClose(object sender, FormClosedEventArgs e)
        {
            this.mainForm.GetMainPictureBox().Image = this.mainForm.currrntImage;
        }
    }
}
