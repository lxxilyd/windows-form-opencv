﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CCWin;
using CCWin.SkinControl;

namespace WindowsFormsApp
{
    public partial class MainForm : CCSkinMain
    {

        private ImageHandle handle;
        public Image currrntImage
        {
            get; set;
        }
        private Image init;
        public MainForm()
        {
            InitializeComponent();
            // 菜单点击事件
            this.openFileMenuItem.Click += new EventHandler(handleOpenFile);
            this.saveMenuItem.Click += new EventHandler(handleSaveFile);
            this.exitMenuItem.Click += new EventHandler(handleExit);
            this.brightMenuItem.Click += new EventHandler(handleBrightSetting);
            this.satutationMenuItem.Click += new EventHandler(handleSatationSetting);
            this.findFaceMenuItem.Click += new EventHandler(handleFindFace);
            this.cameraOnMenuItem.Click += new EventHandler(handelCameraOpen);
            this.cameraOffMenuItem.Click += new EventHandler(handelCameraClose);
            this.faceOnMenuItem.Click += new EventHandler(handleFaceOn);
            this.faceOffMenuItem.Click += new EventHandler(handleFaceOff);
            this.photoMenuItem.Click += new EventHandler(handlePhoto);
            this.BinaryMenuItem.Click += new EventHandler(handleBinarization);
            this.GrayMenuItem.Click += new EventHandler(handleGray);
            this.mainPictureBox.MouseWheel += new MouseEventHandler(pictureBox_MouseWheel);
            this.resetMenuItem.Click += new EventHandler(handleReset);
        }

        /**
         * 开启摄像头方法
         */
        public void handelCameraOpen(object sender, EventArgs e)
        {
            CameraHandle.startCamera();
        }

        /**
         * 开启人脸识别
         */
        public void handleFaceOn(object sender, EventArgs e)
        {
            CameraHandle.findFace = true;
        }

        /**
         * 关闭人脸识别
         */
        public void handleFaceOff(object sender, EventArgs e)
        {
            CameraHandle.findFace = false;
        }

        /**
         * 关闭摄像头方法
         */
        public void handelCameraClose(object sender, EventArgs e)
        {
            CameraHandle.stopCamera();
        }

        /**
         * 拍照
         */
        public void handlePhoto(object sender, EventArgs e)
        {
            this.mainPictureBox.Image = CameraHandle.takePhoto();
        }

        /**
         * 打开文件菜单
         */
        public void handleOpenFile(object sender, EventArgs e)
        {
            String fileName = FileDialogHandle.openFile();
            this.handle = new ImageHandle(fileName);
            this.mainPictureBox.Image = handle.GetBitmap();
            this.currrntImage = handle.GetBitmap();
            this.init = handle.GetBitmap();
        }

        public void handleReset(object sender, EventArgs e)
        {
            this.mainPictureBox.Image = this.init;
            MessageBoxEx.Show("已撤销更改");
        }

        /**
         * 保存文件菜单
         */
        public void handleSaveFile(object sender, EventArgs e)
        {
            String savePath = FileDialogHandle.saveFile();
            if (savePath != null)
            {
                this.mainPictureBox.Image.Save(savePath);
                MessageBoxEx.Show("保存成功！");
            }
        }

        /**
         * 退出菜单
         */
        public void handleExit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /**
         * 图像二值化
         */
        public void handleBinarization(object sender, EventArgs e)
        {
            if (handle == null)
            {
                MessageBoxEx.Show("请先打开一张图片！");
                return;
            }
            this.mainPictureBox.Image = handle.binarization(this.mainPictureBox.Image);
        }

        /**
         * 灰度图
         */
        public void handleGray(object senser, EventArgs e)
        {
            if (handle == null)
            {
                MessageBoxEx.Show("请先打开一张图片！");
                return;
            }
            this.mainPictureBox.Image = handle.grayImage(this.mainPictureBox.Image);
        }

        /**
         * 识别人脸
         */
        public void handleFindFace(object sender, EventArgs e)
        {
            if (handle == null)
            {
                MessageBoxEx.Show("请先打开一张图片！");
                return;
            }
            this.mainPictureBox.Image = handle.findFace(this.mainPictureBox.Image);
        }

        /**
         * 打开亮度窗口
         */
        public void handleBrightSetting(object senser, EventArgs e)
        {
            if (handle == null)
            {
                MessageBoxEx.Show("请先打开一张图片！");
                return;
            }
            BrightForm settingForm = new BrightForm(this);
            settingForm.ShowDialog();
        }

        /**
         * 打开饱和度窗口
         */
        public void handleSatationSetting(object senser, EventArgs e)
        {
            if (handle == null)
            {
                MessageBoxEx.Show("请先打开一张图片！");
                return;
            }
            SaturationForm settingForm = new SaturationForm(this);
            settingForm.ShowDialog();
        }

        /**
         * 自动缩放
         */
        public void autoResize(object sender, EventArgs e)
        {
            //dvpRegion roi;
            //roi = new dvpRegion();
            //dvpStatus status;
            //status = DVPCamera.dvpGetRoi(m_handle, ref roi);

            mainPictureBox.Width = this.Width - mainPictureBox.Left;//显示控件宽度=窗口宽度-显示控件左外边距
            mainPictureBox.Height = this.Height - mainPictureBox.Top;//显示控件高度=窗口高度-显示控件左上边距

            /*维持图像比例*/
            //if (pictureBox1.Width * roi.H > pictureBox1.Height * roi.W)
            //{
            //    pictureBox1.Width = pictureBox1.Height * roi.W / roi.H;
            //}
            //else
            //{
            //    pictureBox1.Height = pictureBox1.Width * roi.H / roi.W;
            //}
        }

        public SkinPictureBox GetMainPictureBox()
        {
            return this.mainPictureBox;
        }

        private void pictureBox_MouseWheel(object sender, MouseEventArgs e)
        {
            this.mainPictureBox.Dock = DockStyle.None;
            this.mainPictureBox.BorderStyle = BorderStyle.FixedSingle;
            Size size = this.mainPictureBox.Size;
            size.Width += e.Delta;
            if (size.Width > mainPictureBox.Image.Width)
            {
                mainPictureBox.Width = mainPictureBox.Image.Width;
                mainPictureBox.Height = mainPictureBox.Image.Height;
            }
            else if (size.Width * mainPictureBox.Image.Height / mainPictureBox.Image.Width < mainPictureBox.Parent.Height - 200)
            {
                return;
            }
            else
            {
                mainPictureBox.Width = size.Width;
                mainPictureBox.Height = size.Width * mainPictureBox.Image.Height / mainPictureBox.Image.Width;
            }
            mainPictureBox.Left = (mainPictureBox.Parent.Width - mainPictureBox.Width) / 2;
            mainPictureBox.Top = (mainPictureBox.Parent.Height - mainPictureBox.Height) / 2;
        }
    }
}
