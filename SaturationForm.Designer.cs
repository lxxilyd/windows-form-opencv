﻿using System.Drawing;

namespace WindowsFormsApp
{
    partial class SaturationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaturationForm));
            this.satationLeb = new System.Windows.Forms.Label();
            this.staurationBar = new CCWin.SkinControl.SkinTrackBar();
            this.hueLab = new System.Windows.Forms.Label();
            this.hueBar = new CCWin.SkinControl.SkinTrackBar();
            this.cancelButton = new CCWin.SkinControl.SkinButton();
            this.okButton = new CCWin.SkinControl.SkinButton();
            ((System.ComponentModel.ISupportInitialize)(this.staurationBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hueBar)).BeginInit();
            this.SuspendLayout();
            // 
            // satationLeb
            // 
            this.satationLeb.AutoSize = true;
            this.satationLeb.Location = new System.Drawing.Point(22, 38);
            this.satationLeb.Name = "satationLeb";
            this.satationLeb.Size = new System.Drawing.Size(68, 15);
            this.satationLeb.TabIndex = 16;
            this.satationLeb.Text = "饱和度0%";
            // 
            // staurationBar
            // 
            this.staurationBar.BackColor = System.Drawing.Color.Transparent;
            this.staurationBar.Bar = null;
            this.staurationBar.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Opacity;
            this.staurationBar.Location = new System.Drawing.Point(79, 19);
            this.staurationBar.Minimum = -100;
            this.staurationBar.Name = "staurationBar";
            this.staurationBar.Size = new System.Drawing.Size(214, 56);
            this.staurationBar.TabIndex = 14;
            this.staurationBar.Track = null;
            // 
            // hueLab
            // 
            this.hueLab.AutoSize = true;
            this.hueLab.Location = new System.Drawing.Point(22, 100);
            this.hueLab.Name = "hueLab";
            this.hueLab.Size = new System.Drawing.Size(45, 15);
            this.hueLab.TabIndex = 19;
            this.hueLab.Text = "色相0";
            // 
            // hueBar
            // 
            this.hueBar.BackColor = System.Drawing.Color.Transparent;
            this.hueBar.Bar = null;
            this.hueBar.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Opacity;
            this.hueBar.Location = new System.Drawing.Point(79, 81);
            this.hueBar.Maximum = 180;
            this.hueBar.Minimum = -180;
            this.hueBar.Name = "hueBar";
            this.hueBar.Size = new System.Drawing.Size(214, 56);
            this.hueBar.TabIndex = 18;
            this.hueBar.Track = null;
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Transparent;
            this.cancelButton.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.cancelButton.DownBack = null;
            this.cancelButton.Location = new System.Drawing.Point(329, 92);
            this.cancelButton.MouseBack = null;
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.NormlBack = null;
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 23;
            this.cancelButton.Text = "取消";
            this.cancelButton.UseVisualStyleBackColor = false;
            // 
            // okButton
            // 
            this.okButton.BackColor = System.Drawing.Color.Transparent;
            this.okButton.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.okButton.DownBack = null;
            this.okButton.Location = new System.Drawing.Point(329, 38);
            this.okButton.MouseBack = null;
            this.okButton.Name = "okButton";
            this.okButton.NormlBack = null;
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 22;
            this.okButton.Text = "确定";
            this.okButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.okButton.UseVisualStyleBackColor = false;
            // 
            // SaturationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(432, 164);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.hueLab);
            this.Controls.Add(this.hueBar);
            this.Controls.Add(this.satationLeb);
            this.Controls.Add(this.staurationBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SaturationForm";
            this.Text = "饱和度/色相";
            ((System.ComponentModel.ISupportInitialize)(this.staurationBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hueBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label satationLeb;
        private CCWin.SkinControl.SkinTrackBar staurationBar;

        private MainForm mainForm;
        private ImageHandle imageHandle;
        private Image currentImage;
        private Image tmpImage;
        private System.Windows.Forms.Label hueLab;
        private CCWin.SkinControl.SkinTrackBar hueBar;
        private CCWin.SkinControl.SkinButton cancelButton;
        private CCWin.SkinControl.SkinButton okButton;
    }
}