﻿using CCWin;
using System;
using System.Windows.Forms;

namespace WindowsFormsApp
{
    public partial class BrightForm : CCSkinMain
    {
        public BrightForm()
        {
            InitializeComponent();
        }

        public BrightForm(MainForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            this.currentImage = mainForm.currrntImage;
            this.imageHandle = new ImageHandle();
            this.brightBar.ValueChanged += new EventHandler(handleBright);
            this.rotateBar.ValueChanged += new EventHandler(handleRotate);
            this.contrastBar.ValueChanged += new EventHandler(handleContrast);
            this.okButton.Click += new EventHandler(handleOK);
            this.cancelButton.Click += new EventHandler(handleCancel);
            this.FormClosed += new FormClosedEventHandler(this.FormClose);
        }

        /*
         * * 亮度调整
         */
        public void handleBright(object senser, EventArgs e)
        {
            this.mainForm.GetMainPictureBox().Image = this.imageHandle.brightAdjust(this.currentImage, this.brightBar.Value);
            this.tmpImage = this.mainForm.GetMainPictureBox().Image;
            this.brightLeb.Text = "亮度" + this.brightBar.Value.ToString() + "%";
        }

        /**
         * 对比度调整
         */
        public void handleContrast(object senser, EventArgs e)
        {
            this.mainForm.GetMainPictureBox().Image = this.imageHandle.contrastAdjust(this.currentImage, this.contrastBar.Value);
            this.tmpImage = this.mainForm.GetMainPictureBox().Image;
            this.contrastLab.Text = "对比度" + this.contrastBar.Value.ToString() + "%";
        }


        /**
         * * 旋转图片
         */
        public void handleRotate(object senser, EventArgs e)
        {
            this.mainForm.GetMainPictureBox().Image = this.imageHandle.rotate(this.currentImage, this.rotateBar.Value);
            this.rotateLeb.Text = this.handleRotateText(this.rotateBar.Value.ToString());
        }

        /**
         * 确定按钮监听
         */
        public void handleOK(object senser, EventArgs e)
        {
            this.mainForm.currrntImage = this.tmpImage;
            this.Close();
        }

        /**
         * 取消按钮监听
         */
        public void handleCancel(object senser, EventArgs e)
        {
            this.currentImage = this.mainForm.currrntImage;
            this.tmpImage = this.mainForm.currrntImage;
            this.mainForm.GetMainPictureBox().Image = this.currentImage;
            this.brightBar.Value = 0;
            this.rotateBar.Value = 0;
            this.contrastBar.Value = 0;
            this.Close();
        }

        public string handleRotateText(string text)
        {
            return "旋转" + text + "度";
        }

        private void FormClose(object sender, FormClosedEventArgs e)
        {
            this.mainForm.GetMainPictureBox().Image = this.mainForm.currrntImage;
        }
    }
}
