﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp
{
    class FileDialogHandle
    {
        /**
         * 打开文件对话框
         */
        public static String openFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "图像文件|*.jpg;*.png;*.bmp;*.jpeg|全部文件|*.*";
            dialog.Multiselect = false;
            dialog.RestoreDirectory = true;
            dialog.FilterIndex = 1;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                String fileName = dialog.FileName;
                return fileName;
            }
            return null;
        }

        /**
         * 保存文件对话框
         */
        public static String saveFile()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "*.jpg|*.png|*.bmp|*.*";
            dialog.RestoreDirectory = true;
            dialog.FilterIndex = 1;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                String fileName = dialog.FileName;
                return fileName;
            }
            return null;
        }
    }
}
