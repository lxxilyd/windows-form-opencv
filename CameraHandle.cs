﻿using System;
using System.Drawing;
using System.Windows.Forms;
using OpenCvSharp;
using CCWin;

namespace WindowsFormsApp
{
    class CameraHandle
    {
        private static bool showCamera = true;
        private static VideoCapture camera;
        private static FaceHandle faceHandle = new FaceHandle();
        private static ImageHandle imageHandle = new ImageHandle();

        public static bool findFace = false;
        private static Mat currentFrame = null;
       
        /**
         * 开启摄像头
         */
        public static void startCamera()
        {
            showCamera = true;
            Mat frame = new Mat();
            Mat frame1 = new Mat();
            try
            {
                camera = new VideoCapture(0);
                
                Cv2.WaitKey(200);
                while (showCamera)
                {
                    camera.Read(frame);
                    Cv2.Flip(frame, frame1, FlipMode.Y);
                    currentFrame = frame1;
                   if (findFace)
                    {
                        faceHandle.findFace(frame1);
                    }
                    Cv2.ImShow("camera", frame1);
                    Cv2.WaitKey(1);
                }
            }catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBoxEx.Show("开启摄像头失败,请确定该设备存在摄像头设备");
            }
        }

        /**
         * 拍照
         */
        public static Image takePhoto()
        {
            if (camera == null || currentFrame == null)
            {
                MessageBoxEx.Show("请先开启相机");
                return null;
            }
            stopCamera();
            return imageHandle.GetBitmap(currentFrame);
        }

        /**
         * 关闭摄像头
         */
        public static void stopCamera()
        {
            showCamera = false;
            try
            {
                Cv2.DestroyWindow("camera");
                if (camera != null)
                {
                    camera.Dispose();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBoxEx.Show(e.Message);
            }
        }
    }
}
