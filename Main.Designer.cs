﻿
using System;
using System.Windows.Forms;
using CCWin.SkinControl;

namespace WindowsFormsApp
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.skinMenuStrip = new CCWin.SkinControl.SkinMenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brightMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.satutationMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BinaryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GrayMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adviceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findFaceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraOnMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraFaceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.faceOnMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.faceOffMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraOffMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.photoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainPictureBox = new CCWin.SkinControl.SkinPictureBox();
            this.skinMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // skinMenuStrip
            // 
            this.skinMenuStrip.Arrow = System.Drawing.Color.Black;
            this.skinMenuStrip.Back = System.Drawing.Color.WhiteSmoke;
            this.skinMenuStrip.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.skinMenuStrip.BackRadius = 4;
            this.skinMenuStrip.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.skinMenuStrip.Base = System.Drawing.Color.WhiteSmoke;
            this.skinMenuStrip.BaseFore = System.Drawing.Color.Black;
            this.skinMenuStrip.BaseForeAnamorphosis = false;
            this.skinMenuStrip.BaseForeAnamorphosisBorder = 4;
            this.skinMenuStrip.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.skinMenuStrip.BaseHoverFore = System.Drawing.Color.White;
            this.skinMenuStrip.BaseItemAnamorphosis = false;
            this.skinMenuStrip.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.BaseItemBorderShow = true;
            this.skinMenuStrip.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("skinMenuStrip.BaseItemDown")));
            this.skinMenuStrip.BaseItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("skinMenuStrip.BaseItemMouse")));
            this.skinMenuStrip.BaseItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.BaseItemRadius = 4;
            this.skinMenuStrip.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinMenuStrip.BaseItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.DropDownImageSeparator = System.Drawing.Color.White;
            this.skinMenuStrip.Fore = System.Drawing.Color.Black;
            this.skinMenuStrip.HoverFore = System.Drawing.Color.White;
            this.skinMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.skinMenuStrip.ItemAnamorphosis = true;
            this.skinMenuStrip.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.ItemBorderShow = true;
            this.skinMenuStrip.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.ItemRadius = 4;
            this.skinMenuStrip.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem,
            this.settingMenuItem,
            this.adviceMenuItem,
            this.CameraToolStripMenuItem});
            this.skinMenuStrip.Location = new System.Drawing.Point(4, 28);
            this.skinMenuStrip.Name = "skinMenuStrip";
            this.skinMenuStrip.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinMenuStrip.Size = new System.Drawing.Size(890, 28);
            this.skinMenuStrip.SkinAllColor = true;
            this.skinMenuStrip.TabIndex = 5;
            this.skinMenuStrip.TitleAnamorphosis = true;
            this.skinMenuStrip.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinMenuStrip.TitleRadius = 4;
            this.skinMenuStrip.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileMenuItem,
            this.saveMenuItem,
            this.exitMenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(53, 24);
            this.fileMenuItem.Text = "文件";
            // 
            // openFileMenuItem
            // 
            this.openFileMenuItem.Name = "openFileMenuItem";
            this.openFileMenuItem.Size = new System.Drawing.Size(122, 26);
            this.openFileMenuItem.Text = "打开";
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(122, 26);
            this.saveMenuItem.Text = "保存";
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(122, 26);
            this.exitMenuItem.Text = "退出";
            // 
            // settingMenuItem
            // 
            this.settingMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.brightMenuItem,
            this.satutationMenuItem,
            this.BinaryMenuItem,
            this.GrayMenuItem,
            this.resetMenuItem});
            this.settingMenuItem.Name = "settingMenuItem";
            this.settingMenuItem.Size = new System.Drawing.Size(53, 24);
            this.settingMenuItem.Text = "调整";
            // 
            // brightMenuItem
            // 
            this.brightMenuItem.Name = "brightMenuItem";
            this.brightMenuItem.Size = new System.Drawing.Size(173, 26);
            this.brightMenuItem.Text = "亮度/对比度";
            // 
            // satutationMenuItem
            // 
            this.satutationMenuItem.Name = "satutationMenuItem";
            this.satutationMenuItem.Size = new System.Drawing.Size(173, 26);
            this.satutationMenuItem.Text = "色相/饱和度";
            // 
            // BinaryMenuItem
            // 
            this.BinaryMenuItem.Name = "BinaryMenuItem";
            this.BinaryMenuItem.Size = new System.Drawing.Size(173, 26);
            this.BinaryMenuItem.Text = "二值化";
            // 
            // GrayMenuItem
            // 
            this.GrayMenuItem.Name = "GrayMenuItem";
            this.GrayMenuItem.Size = new System.Drawing.Size(173, 26);
            this.GrayMenuItem.Text = "灰度图";
            // 
            // resetMenuItem
            // 
            this.resetMenuItem.Name = "resetMenuItem";
            this.resetMenuItem.Size = new System.Drawing.Size(173, 26);
            this.resetMenuItem.Text = "撤销更改";
            // 
            // adviceMenuItem
            // 
            this.adviceMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findFaceMenuItem});
            this.adviceMenuItem.Name = "adviceMenuItem";
            this.adviceMenuItem.Size = new System.Drawing.Size(53, 24);
            this.adviceMenuItem.Text = "高级";
            // 
            // findFaceMenuItem
            // 
            this.findFaceMenuItem.Name = "findFaceMenuItem";
            this.findFaceMenuItem.Size = new System.Drawing.Size(152, 26);
            this.findFaceMenuItem.Text = "识别人脸";
            // 
            // CameraToolStripMenuItem
            // 
            this.CameraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cameraOnMenuItem,
            this.cameraFaceMenuItem,
            this.cameraOffMenuItem,
            this.photoMenuItem});
            this.CameraToolStripMenuItem.Name = "CameraToolStripMenuItem";
            this.CameraToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.CameraToolStripMenuItem.Text = "相机";
            // 
            // cameraOnMenuItem
            // 
            this.cameraOnMenuItem.Name = "cameraOnMenuItem";
            this.cameraOnMenuItem.Size = new System.Drawing.Size(152, 26);
            this.cameraOnMenuItem.Text = "开启";
            // 
            // cameraFaceMenuItem
            // 
            this.cameraFaceMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.faceOnMenuItem,
            this.faceOffMenuItem});
            this.cameraFaceMenuItem.Name = "cameraFaceMenuItem";
            this.cameraFaceMenuItem.Size = new System.Drawing.Size(152, 26);
            this.cameraFaceMenuItem.Text = "人脸识别";
            // 
            // faceOnMenuItem
            // 
            this.faceOnMenuItem.Name = "faceOnMenuItem";
            this.faceOnMenuItem.Size = new System.Drawing.Size(122, 26);
            this.faceOnMenuItem.Text = "开启";
            // 
            // faceOffMenuItem
            // 
            this.faceOffMenuItem.Name = "faceOffMenuItem";
            this.faceOffMenuItem.Size = new System.Drawing.Size(122, 26);
            this.faceOffMenuItem.Text = "关闭";
            // 
            // cameraOffMenuItem
            // 
            this.cameraOffMenuItem.Name = "cameraOffMenuItem";
            this.cameraOffMenuItem.Size = new System.Drawing.Size(152, 26);
            this.cameraOffMenuItem.Text = "关闭";
            // 
            // photoMenuItem
            // 
            this.photoMenuItem.Name = "photoMenuItem";
            this.photoMenuItem.Size = new System.Drawing.Size(152, 26);
            this.photoMenuItem.Text = "拍照";
            // 
            // mainPictureBox
            // 
            this.mainPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.mainPictureBox.Location = new System.Drawing.Point(30, 59);
            this.mainPictureBox.Name = "mainPictureBox";
            this.mainPictureBox.Size = new System.Drawing.Size(811, 419);
            this.mainPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.mainPictureBox.TabIndex = 6;
            this.mainPictureBox.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(898, 505);
            this.Controls.Add(this.mainPictureBox);
            this.Controls.Add(this.skinMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MdiBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Name = "MainForm";
            this.Text = "";
            this.Resize += new System.EventHandler(this.autoResize);
            this.skinMenuStrip.ResumeLayout(false);
            this.skinMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private SkinMenuStrip skinMenuStrip;
        private ToolStripMenuItem fileMenuItem;
        private ToolStripMenuItem openFileMenuItem;
        private SkinPictureBox mainPictureBox;
        private ToolStripMenuItem saveMenuItem;
        private ToolStripMenuItem exitMenuItem;
        private ToolStripMenuItem settingMenuItem;
        private ToolStripMenuItem brightMenuItem;

        private int zoomStep = 1;
        private ToolStripMenuItem satutationMenuItem;
        private ToolStripMenuItem adviceMenuItem;
        private ToolStripMenuItem findFaceMenuItem;
        private ToolStripMenuItem BinaryMenuItem;
        private ToolStripMenuItem GrayMenuItem;
        private ToolStripMenuItem CameraToolStripMenuItem;
        private ToolStripMenuItem cameraOnMenuItem;
        private ToolStripMenuItem cameraFaceMenuItem;
        private ToolStripMenuItem faceOnMenuItem;
        private ToolStripMenuItem faceOffMenuItem;
        private ToolStripMenuItem cameraOffMenuItem;
        private ToolStripMenuItem photoMenuItem;
        private ToolStripMenuItem resetMenuItem;
    }
}

