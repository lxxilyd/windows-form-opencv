﻿using System.Drawing;

namespace WindowsFormsApp
{
    partial class BrightForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BrightForm));
            this.rotateLeb = new System.Windows.Forms.Label();
            this.brightLeb = new System.Windows.Forms.Label();
            this.brightBar = new CCWin.SkinControl.SkinTrackBar();
            this.rotateBar = new CCWin.SkinControl.SkinTrackBar();
            this.contrastLab = new System.Windows.Forms.Label();
            this.contrastBar = new CCWin.SkinControl.SkinTrackBar();
            this.okButton = new CCWin.SkinControl.SkinButton();
            this.cancelButton = new CCWin.SkinControl.SkinButton();
            ((System.ComponentModel.ISupportInitialize)(this.brightBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotateBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contrastBar)).BeginInit();
            this.SuspendLayout();
            // 
            // rotateLeb
            // 
            this.rotateLeb.AutoSize = true;
            this.rotateLeb.Location = new System.Drawing.Point(20, 78);
            this.rotateLeb.Name = "rotateLeb";
            this.rotateLeb.Size = new System.Drawing.Size(60, 15);
            this.rotateLeb.TabIndex = 17;
            this.rotateLeb.Text = "旋转0度";
            // 
            // brightLeb
            // 
            this.brightLeb.AutoSize = true;
            this.brightLeb.Location = new System.Drawing.Point(22, 38);
            this.brightLeb.Name = "brightLeb";
            this.brightLeb.Size = new System.Drawing.Size(53, 15);
            this.brightLeb.TabIndex = 16;
            this.brightLeb.Text = "亮度0%";
            // 
            // brightBar
            // 
            this.brightBar.BackColor = System.Drawing.Color.Transparent;
            this.brightBar.Bar = null;
            this.brightBar.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Opacity;
            this.brightBar.Location = new System.Drawing.Point(79, 19);
            this.brightBar.Maximum = 150;
            this.brightBar.Minimum = -150;
            this.brightBar.Name = "brightBar";
            this.brightBar.Size = new System.Drawing.Size(214, 56);
            this.brightBar.TabIndex = 14;
            this.brightBar.Track = null;
            // 
            // rotateBar
            // 
            this.rotateBar.BackColor = System.Drawing.Color.Transparent;
            this.rotateBar.Bar = null;
            this.rotateBar.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Opacity;
            this.rotateBar.Location = new System.Drawing.Point(79, 57);
            this.rotateBar.Maximum = 360;
            this.rotateBar.Name = "rotateBar";
            this.rotateBar.Size = new System.Drawing.Size(214, 56);
            this.rotateBar.TabIndex = 15;
            this.rotateBar.Track = null;
            // 
            // contrastLab
            // 
            this.contrastLab.AutoSize = true;
            this.contrastLab.Location = new System.Drawing.Point(22, 125);
            this.contrastLab.Name = "contrastLab";
            this.contrastLab.Size = new System.Drawing.Size(68, 15);
            this.contrastLab.TabIndex = 19;
            this.contrastLab.Text = "对比度0%";
            // 
            // contrastBar
            // 
            this.contrastBar.BackColor = System.Drawing.Color.Transparent;
            this.contrastBar.Bar = null;
            this.contrastBar.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Opacity;
            this.contrastBar.Location = new System.Drawing.Point(79, 106);
            this.contrastBar.Minimum = -50;
            this.contrastBar.Name = "contrastBar";
            this.contrastBar.Size = new System.Drawing.Size(214, 56);
            this.contrastBar.TabIndex = 18;
            this.contrastBar.Track = null;
            // 
            // okButton
            // 
            this.okButton.BackColor = System.Drawing.Color.Transparent;
            this.okButton.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.okButton.DownBack = null;
            this.okButton.Location = new System.Drawing.Point(332, 70);
            this.okButton.MouseBack = null;
            this.okButton.Name = "okButton";
            this.okButton.NormlBack = null;
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 20;
            this.okButton.Text = "确定";
            this.okButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.okButton.UseVisualStyleBackColor = false;
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Transparent;
            this.cancelButton.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.cancelButton.DownBack = null;
            this.cancelButton.Location = new System.Drawing.Point(332, 116);
            this.cancelButton.MouseBack = null;
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.NormlBack = null;
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "取消";
            this.cancelButton.UseVisualStyleBackColor = false;
            // 
            // BrightForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(430, 198);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.contrastLab);
            this.Controls.Add(this.contrastBar);
            this.Controls.Add(this.rotateLeb);
            this.Controls.Add(this.brightLeb);
            this.Controls.Add(this.brightBar);
            this.Controls.Add(this.rotateBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BrightForm";
            this.Text = "亮度/对比度";
            ((System.ComponentModel.ISupportInitialize)(this.brightBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotateBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contrastBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label rotateLeb;
        private System.Windows.Forms.Label brightLeb;
        private CCWin.SkinControl.SkinTrackBar brightBar;
        private CCWin.SkinControl.SkinTrackBar rotateBar;

        private MainForm mainForm;
        private ImageHandle imageHandle;
        private Image currentImage;
        private Image tmpImage;
        private System.Windows.Forms.Label contrastLab;
        private CCWin.SkinControl.SkinTrackBar contrastBar;
        private CCWin.SkinControl.SkinButton okButton;
        private CCWin.SkinControl.SkinButton cancelButton;
    }
}