﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Drawing;
using Point = OpenCvSharp.Point;
using ResourceTracker = OpenCvSharp.Util.ResourcesTracker;

namespace WindowsFormsApp
{
    class ImageHandle
    {
        private String imagePath;
        private Mat img;

        private FaceHandle face;

        public ImageHandle()
        {

        }

        public ImageHandle(String imagePath)
        {
            if (imagePath != null)
            {
                this.imagePath = imagePath;
                this.img = new Mat(imagePath, ImreadModes.Unchanged);
            }
        }

        /**
         * 图像二值化方法
         */
        public Bitmap binarization(Image image)
        {
            Mat src = coverToMat(image);
            Mat dst = new Mat();
            Mat grayImage = new Mat();
            // 先转为灰度图
            Cv2.CvtColor(src, grayImage, ColorConversionCodes.BGR2GRAY);
            // 将灰度图进行二值化
            Cv2.Threshold(grayImage, dst, 0, 255, ThresholdTypes.Binary | ThresholdTypes.Otsu);
            return GetBitmap(dst);
        }

        /**
         * 灰度图方法
         */
        public Bitmap grayImage(Image image)
        {
            Mat src = coverToMat(image);
            Mat grayImage = new Mat();
            // 转为灰度图
            Cv2.CvtColor(src, grayImage, ColorConversionCodes.BGR2GRAY);
            return GetBitmap(grayImage);
        }

        /**
         * 亮度调整
         */
        public Bitmap brightAdjust(Image image, float value)
        {
            Mat src = coverToMat(image);
            Mat dst = this.bright(src, value);
            return GetBitmap(dst);
        }

        /**
         * 饱和度调整
         */
        public Bitmap saturation(Image image, float value)
        {
            double raw_value = (value + 100) * 0.01;
            Mat src = coverToMat(image);
            // 转换为HSV空间
            Mat hsv = new Mat();
            Cv2.CvtColor(src, hsv, ColorConversionCodes.BGR2HSV);

            // 提取S通道
            Mat s = new Mat();
            Cv2.ExtractChannel(hsv, s, 1);
            s.ConvertTo(s, -1, raw_value, 0);

            // 将调整后的S通道放回HSV图像，转换回BGR空间
            Cv2.InsertChannel(s, hsv, 1);
            Mat dst = new Mat();
            Cv2.CvtColor(hsv, dst, ColorConversionCodes.HSV2BGR);
            src.Release();
            hsv.Release();
            s.Release();
            return GetBitmap(dst);
        }

        /**
         * 色相调整
         */

        public Bitmap hue(Image image, float value)
        {
            using (ResourceTracker resources = new ResourceTracker())
            {
                Mat src = coverToMat(image);
                Mat hsv = resources.NewMat();
                Cv2.CvtColor(src, hsv, ColorConversionCodes.BGR2HSV);

                // 获取色相通道
                Mat hue = resources.NewMat();
                Cv2.ExtractChannel(hsv, hue, 0);

                // 将色相角度转换为0-360范围内的值
                double hueShift = value / 360 * 255;

                // 调整色相通道
                hue.ConvertTo(hue, -1, 1, hueShift);

                // 将调整后的色相通道合并回HSV图像中
                Cv2.InsertChannel(hue, hsv, 0);
                Mat dst = resources.NewMat();
                Cv2.CvtColor(hsv, dst, ColorConversionCodes.HSV2BGR);
                return GetBitmap(dst);
            }
        }

        public Mat bright(Mat src, float value)
        {
            Mat dst = new Mat(src.Size(), src.Type());
            Mat tmp = new Mat(src.Size(), src.Type());
            // 遍历所有像素
           
            for (int i = 0; i < tmp.Height; i++)
            {
                for (int j = 0; j < tmp.Width; j++)
                {
                    if(tmp.Channels() == 1)
                    {
                        tmp.Set(i, j, (byte)Math.Abs(value));
                    }
                    else
                    {
                        Vec3b color = new Vec3b();
                        color.Item0 = (byte)Math.Abs(value); // B通道（蓝色）
                        color.Item1 = (byte)Math.Abs(value); // G通道（绿色）
                        color.Item2 = (byte)Math.Abs(value); // R通道（红色）
                        tmp.Set(i, j, color);
                    }
                }
            }
            if(value > 0)
            {
                Cv2.Add(src, tmp, dst);
            }
            else
            {
                Cv2.Subtract(src, tmp, dst);
            }
            return dst;
        }

        /**
         * 对比度
         */
        public Bitmap contrastAdjust(Image image, float value)
        {
            Mat src = coverToMat(image);
            Mat dst = contrast(src, value);

            return GetBitmap(dst);
        }

        public Mat contrast(Mat src, float value)
        {
            float value_f = (value + 100) * 0.01f;
            Mat dst = new Mat(src.Size(), src.Type());
            Cv2.ConvertScaleAbs(src, dst, value_f);
            return dst;
        }

        /**
         * 识别人脸
         */
        public Bitmap findFace(Image image)
        {
            Mat src = coverToMat(image);
            if (face == null)
            {
                face = new FaceHandle();
            }
            Mat dst = face.findFace(src);

            return GetBitmap(dst);
        }

        /**
         * 镜像
         */
        public Bitmap overTurn(Image image)
        {
            Mat src = coverToMat(image);
            Mat dst = new Mat();
            Cv2.Flip(src, dst, FlipMode.Y);
            return GetBitmap(dst);
        }

        /**
         * 旋转
         */
        public Bitmap rotate(Image image, double rotate)
        {
            Mat src = coverToMat(image);
            Point center = new Point(src.Cols / 2, src.Rows / 2);
            Mat dst = new Mat();
            Mat m = Cv2.GetRotationMatrix2D(center, rotate, 1);
            Cv2.WarpAffine(src, dst, m, src.Size());
            return GetBitmap(dst);
        }
        private Mat coverToMat(Image image)
        {
            using (ResourceTracker t = new ResourceTracker())
            {
                Bitmap bitmap = new Bitmap(image);
                return BitmapConverter.ToMat(bitmap);
            }
        }

        /**
         * 

        /**
         * Mat对象转Bitmap对象方法
         */
        public Bitmap GetBitmap(Mat mat)
        {
            Bitmap bitmap = BitmapConverter.ToBitmap(mat);
            mat.Release();
            return bitmap;
        }

        /**
         * Mat对象转Bitmap对象方法
         */
        public Bitmap GetBitmap()
        {
            if (this.img != null)
            {
                return BitmapConverter.ToBitmap(this.img);
            }
            return null;
        }

        public float Saturate_cast(float value)
        {
            if(value <= 0)
            {
                return 1;
            }
            if(value > 255) 
            {
                return 255;
            }
            return value;
        }
    }
}
