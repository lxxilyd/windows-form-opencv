﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CCWin;
using CCWin.SkinControl;

namespace WindowsFormsApp
{
    public partial class CV : CCSkinMain
    {

        private ImageHandle handle;
        Image currrntImage;
        public CV()
        {
            InitializeComponent();
            // 菜单点击事件
            this.openFileMenuItem.Click += new EventHandler(handleOpenFile);
            this.saveMenuItem.Click += new EventHandler(handleSaveFile);
            this.exitMenuItem.Click += new EventHandler(handleExit);
            this.brightMenuItem.Click += new EventHandler(handleSetting);
            // 按钮点击事件
            this.startCameraButton.Click += new EventHandler(handelStartButtonClick);
            this.stopCameraButton.Click += new EventHandler(handelStoptButtonClick);
            this.binarizationButton.Click += new EventHandler(handleBinarization);
            this.grayButton.Click += new EventHandler(handleGray);
            this.faceButton.Click += new EventHandler(handleFindFace);
            this.rotateBar.ValueChanged += new EventHandler(handleRotate);
            this.brightBar.ValueChanged += new EventHandler(handleBright);
        }

        /**
         * 开启摄像头方法
         */
        public void handelStartButtonClick(object sender, EventArgs e)
        {
            CameraHandle.startCamera();
        }

        /**
         * 关闭摄像头方法
         */
        public void handelStoptButtonClick(object sender, EventArgs e)
        {
            CameraHandle.stopCamera();
        }

        /**
         * 打开文件菜单
         */
        public void handleOpenFile(object sender, EventArgs e)
        {
            String fileName = FileDialogHandle.openFile();
            //this.pictureBox.Load(fileName);
            this.handle = new ImageHandle(fileName);
            this.mainPictureBox.Image = handle.GetBitmap();
            this.currrntImage = handle.GetBitmap();
            //this.pictureBox.Image = handle.binarization();
        }

        /**
         * 保存文件菜单
         */
        public void handleSaveFile(object sender, EventArgs e)
        {
            String savePath = FileDialogHandle.saveFile();
            // MessageBox.Show(savePath);
            if (savePath != null)
            {
                this.mainPictureBox.Image.Save(savePath);
                MessageBox.Show("保存成功！");
            }
        }

        /**
         * 退出菜单
         */
        public void handleExit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /**
         * 调整菜单
         */
        public void handleSetting(object sender, EventArgs e)
        {
            SettingForm form = new SettingForm();
            form.ShowDialog();
        }

        /**
         * 图像二值化
         */
        public void handleBinarization(object sender, EventArgs e)
        {
            this.mainPictureBox.Image = handle.binarization();
        }

        /**
         * 灰度图
         */
        public void handleGray(object senser, EventArgs e)
        {
            this.mainPictureBox.Image = handle.grayImage(this.mainPictureBox.Image);
        }

        /**
         * 亮度调整
         */
        public void handleBright(object senser, EventArgs e)
        {
            this.mainPictureBox.Image = handle.brightAdjust(this.handle.GetBitmap(), this.brightBar.Value);
            this.brightLeb.Text = "亮度" + this.brightBar.Value.ToString() + "%";
        }

        /**
         * 识别人脸
         */
        public void handleFindFace(object sender, EventArgs e)
        {
            this.mainPictureBox.Image = handle.findFace(this.mainPictureBox.Image);
        }

        /**
         * 旋转图片
         */
        public void handleRotate(object senser, EventArgs e)
        {

            this.mainPictureBox.Image = handle.rotate(this.currrntImage, this.rotateBar.Value);
            this.rotateLeb.Text = this.handleRotateText(this.rotateBar.Value.ToString());
        }

        public string handleRotateText(string text)
        {
            return "旋转" + text + "度";
        }

        public SkinPictureBox GetPictureBox()
        {
            return this.mainPictureBox;
        }
        
    }
}
