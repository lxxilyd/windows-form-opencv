﻿
using System;
using System.Windows.Forms;
using CCWin.SkinControl;

namespace WindowsFormsApp
{
    partial class CV
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CV));
            this.startCameraButton = new CCWin.SkinControl.SkinButton();
            this.stopCameraButton = new CCWin.SkinControl.SkinButton();
            this.skinMenuStrip = new CCWin.SkinControl.SkinMenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox = new CCWin.SkinControl.SkinPictureBox();
            this.binarizationButton = new CCWin.SkinControl.SkinButton();
            this.skinMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // startCameraButton
            // 
            this.startCameraButton.BackColor = System.Drawing.Color.Transparent;
            this.startCameraButton.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.startCameraButton.DownBack = null;
            this.startCameraButton.Location = new System.Drawing.Point(7, 56);
            this.startCameraButton.MouseBack = null;
            this.startCameraButton.Name = "startCameraButton";
            this.startCameraButton.NormlBack = null;
            this.startCameraButton.Size = new System.Drawing.Size(75, 23);
            this.startCameraButton.TabIndex = 3;
            this.startCameraButton.Text = "开启摄像头";
            this.startCameraButton.UseVisualStyleBackColor = false;
            // 
            // stopCameraButton
            // 
            this.stopCameraButton.BackColor = System.Drawing.Color.Transparent;
            this.stopCameraButton.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.stopCameraButton.DownBack = null;
            this.stopCameraButton.Location = new System.Drawing.Point(7, 103);
            this.stopCameraButton.MouseBack = null;
            this.stopCameraButton.Name = "stopCameraButton";
            this.stopCameraButton.NormlBack = null;
            this.stopCameraButton.Size = new System.Drawing.Size(75, 23);
            this.stopCameraButton.TabIndex = 4;
            this.stopCameraButton.Text = "关闭摄像头";
            this.stopCameraButton.UseVisualStyleBackColor = false;
            // 
            // skinMenuStrip
            // 
            this.skinMenuStrip.Arrow = System.Drawing.Color.Black;
            this.skinMenuStrip.Back = System.Drawing.Color.White;
            this.skinMenuStrip.BackRadius = 4;
            this.skinMenuStrip.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.skinMenuStrip.Base = System.Drawing.SystemColors.ControlDarkDark;
            this.skinMenuStrip.BaseFore = System.Drawing.Color.Black;
            this.skinMenuStrip.BaseForeAnamorphosis = false;
            this.skinMenuStrip.BaseForeAnamorphosisBorder = 4;
            this.skinMenuStrip.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.skinMenuStrip.BaseHoverFore = System.Drawing.Color.White;
            this.skinMenuStrip.BaseItemAnamorphosis = true;
            this.skinMenuStrip.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.BaseItemBorderShow = true;
            this.skinMenuStrip.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("skinMenuStrip.BaseItemDown")));
            this.skinMenuStrip.BaseItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("skinMenuStrip.BaseItemMouse")));
            this.skinMenuStrip.BaseItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.BaseItemRadius = 4;
            this.skinMenuStrip.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinMenuStrip.BaseItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinMenuStrip.Fore = System.Drawing.Color.Black;
            this.skinMenuStrip.HoverFore = System.Drawing.Color.White;
            this.skinMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.skinMenuStrip.ItemAnamorphosis = true;
            this.skinMenuStrip.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.ItemBorderShow = true;
            this.skinMenuStrip.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinMenuStrip.ItemRadius = 4;
            this.skinMenuStrip.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem});
            this.skinMenuStrip.Location = new System.Drawing.Point(4, 28);
            this.skinMenuStrip.Name = "skinMenuStrip";
            this.skinMenuStrip.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinMenuStrip.Size = new System.Drawing.Size(890, 28);
            this.skinMenuStrip.SkinAllColor = true;
            this.skinMenuStrip.TabIndex = 5;
            this.skinMenuStrip.TitleAnamorphosis = true;
            this.skinMenuStrip.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinMenuStrip.TitleRadius = 4;
            this.skinMenuStrip.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileMenuItem,
            this.saveMenuItem,
            this.exitMenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(53, 24);
            this.fileMenuItem.Text = "文件";
            // 
            // openFileMenuItem
            // 
            this.openFileMenuItem.Name = "openFileMenuItem";
            this.openFileMenuItem.Size = new System.Drawing.Size(122, 26);
            this.openFileMenuItem.Text = "打开";
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(122, 26);
            this.saveMenuItem.Text = "保存";
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(122, 26);
            this.exitMenuItem.Text = "退出";
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox.Location = new System.Drawing.Point(120, 59);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(659, 377);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 6;
            this.pictureBox.TabStop = false;
            // 
            // binarizationButton
            // 
            this.binarizationButton.BackColor = System.Drawing.Color.Transparent;
            this.binarizationButton.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.binarizationButton.DownBack = null;
            this.binarizationButton.Location = new System.Drawing.Point(7, 146);
            this.binarizationButton.MouseBack = null;
            this.binarizationButton.Name = "binarizationButton";
            this.binarizationButton.NormlBack = null;
            this.binarizationButton.Size = new System.Drawing.Size(75, 23);
            this.binarizationButton.TabIndex = 7;
            this.binarizationButton.Text = "二值化";
            this.binarizationButton.UseVisualStyleBackColor = false;
            // 
            // CV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(898, 473);
            this.ControlBoxDeactive = System.Drawing.SystemColors.ControlDarkDark;
            this.Controls.Add(this.binarizationButton);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.stopCameraButton);
            this.Controls.Add(this.startCameraButton);
            this.Controls.Add(this.skinMenuStrip);
            this.Name = "CV";
            this.Text = "CV";
            this.skinMenuStrip.ResumeLayout(false);
            this.skinMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private SkinButton startCameraButton;
        private SkinButton stopCameraButton;
        private SkinMenuStrip skinMenuStrip;
        private ToolStripMenuItem fileMenuItem;
        private ToolStripMenuItem openFileMenuItem;
        private SkinPictureBox pictureBox;
        private SkinButton binarizationButton;
        private ToolStripMenuItem saveMenuItem;
        private ToolStripMenuItem exitMenuItem;
    }
}

